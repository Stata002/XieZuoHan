# 从0开始stata之数据导入与整理 
---
谢作翰 

---
# 1.数据的导入


## 1.1打开本地或网络数据文件

*命令*：` use [varlist] [if] [in] usingfilename [, clear nolabel ]`

*解释*：` use `是打开数据的命令语句，` varlist `代表变量名称，`if`是条件语句，` in `是范围语句，` using filename `代表数据文件路径。

**打开文件中全部数据**

如果想要打开 usaauto 数据文件中的全部数据，输入标准命令如：
`
 use "D:\360Downloads\PanDownload\操作数据\教程数据\chap02\date.dta",clear `

*说明*：` use `是代表打开的命令语句，引号中` D:\360Downloads\PanDownload\操作数据\教程数据\chap02\date.dta `则给出了数据文件的路径

**打开文件中部分变量**

有时，并不需要将数据文件中的所有变量全部打开，因为原始数据内容丰富，含有很多变量，而研究可能只涉及其中的几个变量。所以若只打开` usaauto `文件中的` make `和` price `这两个变量，应该使用如下命令：

` use make price using “C:\data\usaauto.dta `

这个命令中` use make price `部分表示需要打开` make `和` price `两个变量，` using “C:\data\usaauto.dta” `部分表示打开的数据文件路径及名称，如果用户使用此命令打开其他数据文件，所应用的命令相似，只需要把表示文件名称和变量的具体内容修改即可

**打开文件中部分样本**

有时，原始数据文件的样本数量过于庞大，例如人口普查的数据动辄千百万，可是一般的研究大部分不需要全部的样本，只需要部分样本即可，所以这时候只需要打开部分样本。**例如，只需要打开` usaauto `数据文件中第五到第十个样本的数据，可以使用如下命令**：

` use “C:\data\usaauto.dta” in 5\10 `

解释：其中use“C:\data\usaauto.dta”部分表示打开的数据文件名称及路径，in5\10部分表示选取的样本序号，即选取第5到第10个样本。如果用户使用此命令打开其他数据文件，所应用的命令相似，只需要把表示文件名称和样本序号的具体内容修改即可。

**打开具有某些特征的样本**

有时，原始数据将不同特征的样本混杂在一起，而现实的研究却要求将不同的样本分开研究，例如分别研究男性、女性的情况，城市、农村的经济问题，等等。这时就需要只打开具有这些特征的样本数据进行分析，在这个试验中，打开` usaauto `文件中进口车样本数据的命令为：

` use “C:\data\usaauto.dta” if foreign==1 `

*解释*：这个命令语句中最重要的就是` if `语句，该命令执行的结果就是让
 stata 仅仅读入符合条件的样本数据。在本例中，` foreign==1 `就表示是进口车，所以打开的数据就是进口车的数据

## 1.2 导入ASCII数据
当数据文件为其他格式时，也可以导入Stata软件中进行处理，常用的命令主要有` insheet `、` infile `、` infix `命令

**insheet命令——读取ASCII数据**

 **ASCII数据**：` ASCII `数据是指原始的文本数据，是由由电子表格和数据库程序生成的数据文件中，每一行代表一个观测值，数值由逗号或制表符隔开，第一行可以包含变量名称。
基本命令语句：
` insheet [varlsit] using filename [,options] `
*说明*：` insheet `代表导入数据的命令，` [varlsit] using filename `代表数据文件中的某个变量，这里的` options `的具体内容显示在表 2.9 中，主要包括选项的内容和所代表的含义。
 ![](http://upload-images.jianshu.io/upload_images/1844375-4968d624adf73c96.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

*例子*：将数据` citywater.csv `导入Stata中，就不能直接使用` use `命令了，因为此数据文件的后缀名不是` .dta `，而是` .csv `，这种数据类型表示使用逗号分隔的一种数据类型:

` insheet using C:\data\citywater.csv `

` insheet `是导入此类数据的命令，` using C:\data\citywater.csv `指明了数据的路径，此时数据文件` citywater.csv `就被正确导入Stata中，可以进行各种操作了

**infile命令——读取无固定格式命令**

` infile `在某种程度上可以完成与` insheet `命令相同的功能，最大区别是` infile `必须指明变量名称，尤其是字符型变量。而用` infile `命令读取数据的基本命令语句如下：
` infile varlist [_skip[(#)] [varlist [_skip[(#)] ...]]] using filename [if] [in] [, options] `
例如，同样将数据` citywater.csv `导入Stata中，` infile `操作命令为：
` infile year production capital labor using C:\data\citywater.csv `

**infix——读取固定格式数据**

固定格式的数据是指有固定的位数，当位数不够时，前面用 0 补齐，对于这种数据是用` infix `命令读入的，具体形式如下：

` infix using dfilename [if] [in] [, using(filename2) clear]
infix specifications using filename [if] [in] [, clear] `

例如，将下列一组数据（数据文件` chengji.csv `）转化成如表 2.10 所示的数据形式。` chengji `这组数据为用逗号隔开的数据类型，如图 2.11 所示，其中` gender `（性别）只有 0 和 1 两个数字组成，` number `（学号）这一栏必须由三位数组成，math和english的成绩必须由两位数组成，所以这是一个固定格式的数据，应该使用infix命令。
 ![](http://upload-images.jianshu.io/upload_images/1844375-cf800b0d9273fed2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

具体的命令语句为：
` infix gender 1 number 3-5 math 7-8 english 10-11 using C:\data\chengji.csv, clear `
在这个命令语句中，` infix gender 1 `说明第一位数据为性别，` number 3-5 `表示第 3 至 5 位表示学号，` math 7-8 `表示第 7 至 8 位表示数学成绩，` english 10-11 `表示第 10 至 11 为表示英语成绩，` using C:\data\chengji.csv `表示原始数据文件的路径。
# 2 .数据的整理
## 2.1 数据标签
为了让用户更加清楚地了解数据的来源、变量的含义、观测值的解释等相关内容，Stata可以为数据、变量、观测值添加标签，其实标签就是对相关数据的解释。最常用的标签方法有三类：一是为数据库添加标签，二是为变量添加标签，三是为观测值添加标签

1. **为数据库添加标签**

*命令*：` label data ["label"] `

在这个命令语句中，` label data `是为数据库添加标签的命令语句，` ["label"] `代表所要添加的标签的内容

2. **为变量添加标签** 
 
*命令*：` label variable varname ["label"] `
` label variable `是为变量添加标签的命令语句，` varname `代表所要添加标签的变量名称，` ["label"] `代表所要添加的标签的内容。
 
3. **为数值添加标签**

对于数值型的分类变量，单纯通过数值很难判断其含义，例如用 0 和 1 区分性别，但是很难区分 0 是代表男性还是女性，这时如果给数值添加标签，就方便用户理解了。

为数值添加标签是通过两步来完成的，**第一步是定义数值标签**:

*命令为*：` label define lblname # "label" [# "label" ...] [, add modify nofix] `

*说明*：` label define `是定义数值标签的命令语句` lblname `代表所要定义的数值标签的名称，` #  `代表所要定义的数值，` "label" `代表所要添加的标签的内容。需要用户注意的是后方` options `的内容，其中` add `的作用是添加标签内容，` modify `的作用是对已存在的标签内容做修改，` nofix `的作用是要求` Stata `不为标签的内容而改变原变量的存储容量
   第二步是**将所定义的数据标签与相关变量结合**，
*基本命令为*：

` label values varname [lblname] [, nofix] `

在这个命令语句中，` label values `是将定义的数据标签与相关变量结合的语句，` varname `代表将要添加标签的变量名称，` [lblname] `代表刚刚定义的数据标签名称。

  通过下面的例子——给` usaauto.dta `数据库添加三类标签，为大家介绍标签的使用方法，` usaauto `数据文件如表所示
![usaauto](http://upload-images.jianshu.io/upload_images/1844375-bbf8f7146f332c38.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 
导入数据：` use "D:\360Downloads\PanDownload\操作数据\教程数据\chap02\usaauto.dta", clear `

①为整个数据库添加标签“1978年美国汽车产业的横截面数据”

其命令如下：

` label data “1978年美国汽车产业的横截面数据”  `

这个命令语句中` label data `表示为数据库添加标签，` “1978年美国汽车产业的横截面数据” `指明了标签的内容。
 ![数据库标签](http://upload-images.jianshu.io/upload_images/1844375-30096efe4cd3ba2a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

②为变量` make `和` mpg `添加标签“品牌”和“每加仑油行使里程数”，

其命令如下：
```
label variable make “品牌”
label variable mpg “每加仑油行使里程数”
```
` label variable `表示为变量添加标签，` make ` “品牌”表示要添加标签的变量和标签的内容。

 ![变量标签](http://upload-images.jianshu.io/upload_images/1844375-676a188fc107257f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

③  为数值添加标签

变量` foreign `为分类变量，**0** 代表国产,**1** 代表进口，为了便于观察，为**0** 和**1** 添加标签，

具体命令为：
  ```
label define foreignlabel 0 “Domestic” 1 “Foreign”
label values foreign foreignlabel
```


` label define `表示定义标签的内容，` foreignlabel `表示标签的名称,` 0  “Domestic” 1 “Foreign” `表示定义的规则，数字**0**的标签是**国产车**，数字**1**的标签是**进口车**。
![设置前](http://upload-images.jianshu.io/upload_images/1844375-41c8a3e2906169cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![设置后](http://upload-images.jianshu.io/upload_images/1844375-7eeb8e24ff90d70f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

` label values `表示将定义的标签与变量结合的命令，所要结合的变量为` foreign `，结合的标签为` foreignlabel `。标签添加完成以后，可以通过` label dir `命令，查看已经建立标签的相关内容。

## 2.2 数据排序

为了处理数据的方便，有时需要对数据进行排序处理，排序的命令有主要有两个，一个是` sort `命令，一个是` gsort `命令

` sort `命令的基本语句是：

` sort varlist [in] [, stable] `

在这个命令语句中，` sort `是基本命令，` varlist `代表将要进行排序的变量名称，` [in] `代表排序的范围，` [, stable] `的含义是如果两个观测值相同，其顺序保持与原数据相同。
` gsort ` 命令的基本语句是：

` gsort [+|-] varname [[+|-] varname ...] [, generate(newvar) mfirst] `

其中需要说明的内容有两点：
* ` [+] `表示按升序排列，是` Stata `默认的排列方式，`[-]`表示按降序排列
* ` generate(newvar) `表示排序之后生成新的变量，` mfirst `表示将缺失值排在最前面。

将` usaauto `数据文件中的观测值按变量` price `由小到大排列，这个操作可以用` sort `命令完成，具体操作如下：

` sort price `

当然也可以用gsort命令完成，具体操作如下：

` gsort + price `

## 2.3 数据拆分

1. **数据的横向拆分**

原始数据有时包含过多的变量，但在实际应用中可能根据需要将原始数据拆分为不同的数据表，这时就要实现数据的横向拆分。数据的横向拆分用到的两个命令为` drop `和` keep `，` drop `命令是用来**删除某些变量和观测值**的，基本命令如下：

` drop varlist [if] [in] `

` keep `命令是用来**保留某些变量和观测值**的，基本命令如下：

` keep varlist [if] [in] `

 *举例说明* ：` water `数据文件包含四个变量，分别是` year `、` capital `、` production `、` labor `，将` water `数据文件拆分为两个数据文件，一个数据文件包含` year `和` production `两个变量，命名` wateroutput `，一个数据文件包含` year `、` capital `和` labor `三个变量，命名为` waterinput `。这个操作的具体命令如下：
```
use c:\data\water,clear
drop capital labor
save c:\data\wateroutput, replace
```
以上命令，第一个命令实现了原数据文件的打开，第二个命令删除了变量` capital `和` labor `，第三个命令实现了存储，新文件的名字为` wateroutput `，并用` replace `命令替换了原有数据。
```
	use c:\data\water,clear
	keep year capital labor
	save c:\data\waterinput, replace
```
以上命令，第一个命令实现了原数据文件的打开，第二个命令保留了变量` capital `和` labor `，第三个命令实现了存储，新文件的名字为` waterinput `，并用` replace `命令替换了原有数据
 
2. **数据的纵向拆分**

原始数据有时包含过多的样本观测值，但在实际应用中可能根据需要将其按某种特征拆分为不同的数据表，这是就要实现数据的纵向拆分。数据的纵向拆分用到的主要命令还是` drop `和` keep `。

例如将` usaauto `数据文件拆分为两个数据文件，一个数据文件叫` domesticauto `，只包含国产车的相关内容，一个数据文件叫` foreignauto `，只包含进口车的相关内容，具体操作如下：
```
use c:\data\usaauto,clear
drop if foreign==1
save c:\data\domesticauto, replace 
```
以上命令完成了第一个数据文件的建立，第一个命令完成了原始数据文件的打开，第二个命令删除` foreign `变量为 1 的数据，第三个命令存储新的数据文件，名称为` domesticauto `，并用` replace `命令替换了原有数据

第二个数据文件建立的命令如下所示：
```
	use c:\data\usaauto,clear
	keep if foreign==1
	save c:\data\foreignauto, replace
```
以上命令完成了第二个数据文件的建立，第一个命令完成了原始数据文件的打开，第二个命令保留了` foreign `变量为 1 的数据，第三个命令存储新的数据文件，名称为` foreignauto `，并用` replace `命令替换了原有数据
 
## 2.4 数据合并
1. **数据的横向合并**

数据的横向合并是横向拆分的逆操作，但是其要比拆分复杂，因为合并时要实现同一个体数据的对接，而不能出现对接错误的情况。所以在横向合并之前最好先对数据进行排序处理，然后实现合并。合并所使用的命令语句为 `merge `，具体语句如下所示：

`
merge [varlist] using filename [filename ...] [, options]` 

其中，` merge `为合并的命令语句，` [varlist] `代表合并进去的新变量，` using filename `指的是所要与原文件合并的文件路径，` options `包含较多的功能，表 2.11 显示了其具体内容。
 ![](http://upload-images.jianshu.io/upload_images/1844375-a34f88966686e4a8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

*举例*：利用横向拆分实验中生成的数据文件` waterinput `和` wateroutput
 `实现数据的横向合并，匹配变量为` year `，生成新的数据文件命名为` waternew `。这个操作的命令为：
```
use c:\data\wateroutput, clear
sort year
save c:\data\wateroutput, replace
use c:\data\waterinput, clear
sort year
merge year using c:\data\wateroutput
save c:\data\waternew, replace
```
在以上命令语句中，第一个命令语句实现了` wateroutput `数据文件的打开，第二个命令语句将文件按年份变量进行排序，第三个命令语句保存了排序之后的数据文件，第四个命令语句实现了` waterinput `数据文件的打开，第五个命令语句将此数据按年份变量进行排序，第六个命令语句按年份变量将` wateroutput `文件合并到` waterinput `文件中，第七个命令语句保存合并之后的数据文件。
2. **数据的纵向合并**

数据的纵向合并为数据纵向拆分的逆操作，使用的主要命令为` append `命令，具体语句如下：

` append using filename [, options] `

在这个命令语句中，` append `是进行纵向合并的命令语句，` using filename `是进行纵向合并的文件路径，` [, options] `的内容与` merge `相似，但更为简化。

例如，利用纵向拆分实验中生成的数据文` domesticauto `和` foreignauto
 `实现数据的纵向合并，生成的数据文件命名为` usaautonew `。这个操作的命令为：
```
use c:\data\domesticauto, clear
append using c:\data\foreignauto
save c:\data\usaautonew, replace
```
在以上命令中，第一个命令语句打开了原始数据文件，第二个命令将` foreignauto `文件合并到` domesticauto `文件中，第三个命令语句存储了合并后的数据文件。
## 2.4 长宽数据转换
在**面板数据**中，如果包含两个**标识变量**，则数据又两种表现形式，一种是长数据，一种是宽数据。在长宽数据的转换中，所使用到的命令为` reshape `命令，具体命令语句为：
```
reshape long stubnames, i(varlist) [options]
reshape wide stubnames, i(varlist) [options]
```
在这个命令语句中，` reshape `是代表数据转换的命令，` long `表示将宽数据转化为长数据，` wide `表示将长数据转化成宽数据，` stubnames `表示需要转化的变量的名称前缀，` i(varlist) `表示识别变量。` options `最常用的为` j(varname [values]) `，它表示用来进行长宽变换的变量名称，通常为时间变量。

将宽数据转化为长数据的操作命令如下：
```
use c:\data\widedata, clear
 
reshape long english science, i(number name) j(year)
 ```
 ![宽数据](http://upload-images.jianshu.io/upload_images/1844375-8866dff041f3f560.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![转换过程](http://upload-images.jianshu.io/upload_images/1844375-50dcd29621ed7a0c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![长数据](http://upload-images.jianshu.io/upload_images/1844375-ff32fcb94c968c4d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


在以上命令中，` reshape long ` 表示将宽数据转化成长数据的命令语句，` english `和` science `表示将要转化的变量名称的前缀，也即将要生成的变量的名称，` i(number name) ` 表示识别变量，即按学号` number `和名称` name `区分所有观测值，` j(year) `表示将要长宽转换的变量，一般是按年份进行转化。

  将长数据转化为宽数据的操作命令如下：
```
use c:\data\longdata, clear
 
reshape wide english science, i(number name) j(year)
 ```
 ![长数据](http://upload-images.jianshu.io/upload_images/1844375-840d40971cffe15d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![转换过程](http://upload-images.jianshu.io/upload_images/1844375-227f3e4f2b906883.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![宽数据](http://upload-images.jianshu.io/upload_images/1844375-97238d83512f9623.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在以上命令中，` reshape wide `表示将长数据转化成宽数据的命令语句，` english `和` science `表示将要转化的变量名称的前缀，也即将要生成的变量的名称，` i(number name) ` 表示识别变量，即按学号` number `和名称` name `区分所有观测值，` j(year) `表示将要长宽转换的变量，一般是按年份进行转化。

数据文件：链接: https://pan.baidu.com/s/1qXRh9EG  密码: 5ltw


