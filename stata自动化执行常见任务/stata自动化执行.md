 如何用使用Stata自动化执行常见任务
---
>作者：谢作翰 | 连玉君 | (知乎 | 简书 | 码云)
---

> 原文地址 ：https://blog.stata.com/2018/10/09/how-to-automate-common-tasks/


高效进行数据分析的关键是将常见任务自动化。自动化能把你从大量重复性操作中拯救出来，不但节约时间，还能减少出错概率。

本篇让我们使用Stata自动执行一些任务。任务本身并不重要，重要的是熟悉自动化执行任务的流程。

我们选择的任务是将变量**标准化**——**减去变量的均值并除以其标准差**。

正如您所知道的，Stata社区中有现成的命令来执行此操作，而且比我们接下来自己写更为便捷（在Stata中搜索 **normalize variable**）；您也可以使用Stata的**egen**命令对单个变量标准化，但我们打算做的远不止如此。

我认为本文读者是Stata自动化任务的新手。所以，如果您已经是专家，可能对这篇文章不感兴趣，当然也可能会一些新的发现。

**脚本**

首先，我们将直接在分析脚本执行标准化操作。在Stata中，我们将分析脚本称为do-files，因为它们可以执行某些操作。

让我们把原始变量命名为**x**。因为我们不想改变现有变量的内容，所有新建一个变量**xN**，其中**N**后缀表示标准化。（如果您不喜欢**N**后缀，可以改变，比如`_norm`，也可使用前缀。）Stata的**summarize**命令将给出原始变量平均值和标准差。

**mydo.do**



```
···
summarize x
generate xN = (x - r(mean)) / r(sd)
···
```

如上，对一个变量标准化只需要两行代码即可。

**r(mean)** 和 **r（sd)** 是什么意思，我们又该如何了解它们？

这两个分别表示变量均值和标准差的返回值。

在Stata中，几乎所有命令都会返回结果。估计性（**Estimation**）命令返回结果以**e（）** 表示，大多数其他命令以**r（）** 值返回。输入**help summarize**并拉到帮助文件的底部就能看到**summarize**返回的所有结果及其描述。也可以在**summaryrize**命令后输入简单的**返回列表**。**返回清单**向我们显示每个返回的结果及其值。或者我可以进入完整的手册查看**summarize**所有返回结果。您甚至不需要Stata来查看帮助文件或文档。

查看帮助文件，

[https://www.stata.com/help.cgi?summarize](https://www.stata.com/help.cgi?summarize)

或者查看手册输入，

[https://www.stata.com/manuals/rsummarize.pdf](https://www.stata.com/manuals/rsummarize.pdf)

这是快捷的方式。如果您只想浏览Stata的文档，请单击

[https://www.stata-press.com/manuals/documentation-set/](https://www.stata-press.com/manuals/documentation-set/)

单击您感兴趣的手册，然后浏览目录。

抱歉扯的有些远了，但这非常重要。现在回到我们的脚本。

**既然我们的任务只有两行，那为什么我们要自动化呢？**

因为当我们需要大量重复操作时，即使只有两行代码，也极易出错。比如，我们想复制这段代码100次进行100个变量标准化时，我们必须将代码中变量名**x**更改为100个新的变量名称，但是我们往往会忘记。你可能会忘记更改**summarize**中的**x**，或是忘记更改**xN**，并收到报错消息。或是忘记更改在表达式中的变量名，这三处错误我都犯过。



**使用Do文件自动化执行**

我们将脚本放入自己的do-file中。

**normalize.do** (1)



```
version 15.1

summarize x
generate xN = (x - r(mean)) / r(sd)
```



**ps**：在文件顶部我添加了版本命令。请切记，一定要为你的do文件标明版本信息！我使用的是Stata 15.1，一旦标记上，这个脚本将始终以15.1版**stata**的特性运行，即使将来用Stata 42版运行这个文件（可能42版的**stata**早已取消**summarize**命令或完全改变**summarize的**工作方式）但**stata**会识别出版本号，并按15.1版本的特性正常运行。

我们通过输入以下命令执行所写的脚本

```
. do normalize
```

或者在直接在do文件里添加**do normalize**语句。

我们目前的**normalize.do**并不太有趣。我们需要它来处理除**x**之外的变量。

这是一个版本：

**normalize.do** (2)



```
version 15.1

summarize `1'
generate `1'N = (`1' - r(mean)) / r(sd)
```

然后输入

```
. do normalize y
```

从（1）到（2）的变化是什么？我们所做的只是用 **`1′**

替换每次出现的**x** 。为什么是 **'1'** ？Stata的do-files会将其参数依次放进编号为1,2,3等的局部暂元进行解析。第一个参数进入局部暂元`` `1' ``， 第二个参数进入 `` `2' ``，依此类推。什么是局部暂元？暂元是一个存储价值的容器。**1**可以是暂元名称。为什么我们用`` `'``包裹1？因为如果我们只输入1，那指的是数字1.而我们需要仓库**1**中的值，所以我们需要对它解引用。因为我们输入了 **y** ——我们的第一个（也是唯一的）参数，所以暂元**`1'**解引为（**deference**）**y**。如果你不喜欢“解引用”这个表达，也可以说 **“1”** 扩展为 **y** 。

若你在我们的第二版**normalize.do**中以 **y** 代替 **'1'** ，它就成了第一个版本。这也正是Stata所做的。

使用我们的新**normalize.do**，我们可以方便的输入：

```
. do normalize myvariable
. do normalize myothervariable
. do normalize x1
. do normalize x2
...
. do normalize x100
```

使用以上代码我们犯错率大大减小了，只是代码非常冗赘。这个问题稍后我们回来解决。

现在我现在想探究的是，能否在我们的do文件中加入**stata** 的**if**限定符，答案是肯定的，而且很容易。

**为什么我们想要**if**限定词？**

```
. do normalize income if male == 0
```



我们可能想对样本中的女性对象标准化，这时候就可以使用**if**限定符**if male== 0**。

以下是包含**if**和**in**限定符的do文件。

（如果你不知道什么是**in**限定词，点击[https://www.stata.com/help.cgi?in](https://www.stata.com/help.cgi?in)）

**normalize.do** (3)



```
version 15.1

syntax varlist(min=1 max=1) [if] [in]

summarize `varlist' `if' `in'
generate `varlist'N = (`varlist' - r(mean)) / r(sd)   `if' `in'
```



最后两行代码与之前版本相比有所改变，主要在两个方面：第一， 局部暂元 **'1'** 更换为暂元**`` `varlist’ ``** 第二，两个命令结尾部分加入 `if' `、`in' 限定符。我们的do-file现在直接支持**if**和**in**限定符，所以新的**syntax**命令似乎表现出很多魔力，事实上确实如此。

**syntax**解析命令看起来像是标准的Stata命令——有一个变量列表（命令_varlist中_），可选的**if**限定词，可选的**in**限定词，除了缺少选项部分（option）其他都有了。

**syntax**命令的真正优点在于您只需输入完成任务最基本的命令，并且**syntax**解析命令行，使用相关的语法部分填充局部暂元。当输入的内容与您指定的语法不匹配时，它还会发出错误消息。这就是为什么我们不计麻烦在**synta**命令**varlist**参数上添加**（min = 1 max = 1）** ，若不加入括号部分，**syntax**允许指定多个变量。而现在我们只需要一个变量。**[if]** 和 **[in]** 限定符是可选参数。如果语句中 **if** 不带有方括号，此时**if**限定词变为语法必选的参数。

前两个do文件有个问题我们此前忽略了——我从未检查过 **`1'** 是一个未缩写的变量名。Stata允许缩写变量名称。如果你有一个名为 **foreign** 的变量，并且没有其他变量名的缩写是 **for**，此时输入

```
. do normalize for
```

此时会创建一个名为**forN**的新变量，而不是**foreignN**。无论如何，你必须要小心。一些方法可以解决这个问题，但我们不再过多介绍。

使用**syntax**语句则没有这个问题。即使在命令行中输入**for**，局部暂元**`varlist'** 也会扩展为未缩写的变量名**foreign**。这也是**syntax**的厉害之处。

现在，让我们来解决代码冗赘问题。

如果我们想要标准化的变量数量很多怎么办？这也很容易，但我们必须最终添加到我们的两行计算代码中。

这是一个do-file，它接受变量列表并对每个变量进行规范化，同时支持**if**和**in**限定符。

**normalize.do** (4)

```
version 15.1

syntax varlist [if] [in]

foreach var in `varlist' {
    summarize `var' `if' `in'
    generate `var'N = (`var' - r(mean)) / r(sd)   `if' `in'
}
```



 在顶部的syntax语句格式行，我们删除了**（min = 1 max = 1）**，因为现在我们要接受含多个变量的列表 **varlist** 。

**foreach**，命令很容易理解。对每个属于变量列表（**varlist**）中的变量**var**依次执行标准化命令。暂元**`` `varlist' ``** 解析为do所指定的变量列表。**var** 是用来保存单个变量名称的代号，因为我们一次只能循环一个变量。我们也可以使用其他代号如**变量**，**v**，**z**。如果您对暂元的概念不熟悉，不妨将其理解为空一个容器或仓库，在程序解析时才被赋予为特定的值（这一过程又称解引用或拓展）。以上代码中**`` `varlist' ``** 仓库中存放的是由环境中多个变量名组成的列表，而仓库** `var' **存放的是某单个变量名，并且var这个仓库里存放的变量名会随着循环次数的变化改变，但每次都只能存放一位，一直到遍历仓库varlist中的所有变量名。



我们现在输入

```
. do normalize x1 x2 x3  if male==0
```

或者

```
. do normalize x*
```

**normalize.do**将从Stata *varlist*中接受符合条件的变量。如果您还了解**varlist**，请单击<https://www.stata.com/help.cgi?varlist>查看所有含义。

**创建一个新命令**

这小小的自动化过程给我们带来了许多方便，我们可能不满足仅仅把它保存为一个do文件，而想要将它变成一个新的stata命令，这样我们可以在任何项目中使用它，甚至可以与我们的同事分享。

再说一次，这并不难。

我们将创建一个ado文件（自动化的do文件）。在ado文件中定义的程序就像Stata内置命令一样，会自动被发现并运行。



**normalize.ado** (a)



```
program normalize
    version 15.1

    syntax varlist [if] [in]

    foreach var in `varlist' {
        summarize `var' `if' `in'
        generate `var'N = (`var' - r(mean)) / r(sd)   `if' `in'
    }
end
```

我们做了哪些调整呢？

1）我们对version（4）代码进行了缩进，但这仅仅只是为了美观。2）我们在文件顶部添加了**program normalize**，底部添加了**end**。这样一来stata会将其视为命令，因此我们**不必**在运行时先输入**do**。



我们现在有一个程序，只要我们输入**normalize**就会自动发现并运行它。

我们现在可以输入

```
. normalize x1 x2 x3  if male==0
```

要么

```
. normalize x*
```

我们可以将文件**normalize.ado**提供同事，它也适用于他们。

现在行动起来，自动化执行自己的任务吧！

**一些簿记**

如果您将**normalize.ado**放在可以被stata发现的路径中（如当前的工作目录）它的确足以应付日常的自动化需求。但您可能不希望将其放在每个工作目录中。而且万一我们对命令改动升级了怎么办？我们还得在好几个几个地方改变它。这时，在Stata中输入

```
.adopath
```

该路径上的一个目录将被标记为**（PERSONAL）**。复制**normalize.ado**到那里。无论您在哪个目录中工作，现在都可以在所有项目中找到它。

如果您将**normalize.ado**提供给同事，请告诉他们将其复制到他们的**（PERSONAL）**目录。

事实上，我并不总是采用自动化。我发现不同情况下止步于我们的do文件的版本（1），（2），（3）或（4）都是有用的。或者一直到新的命令。

另外，我们将程序名叫做**normalize**并将其放入名为**normalize.ado**文件中并非随意。**程序名称和文件名必须相同**。

还有一个细节。每次键入**do normalize** ...时，都会从**normalize.do**文件重新加载您的文件。键入**normalize**后，您的ado文件程序将保留在Stata的内存中。下次键入**normalize时**，Stata会从内存运行程序而不重新读取**normalize.ado**文件。那更快。但是......如果您正在调试程序并编辑文件，则不会重新加载您的更改。在键入**normalize**之前，您需要键入**discard** .... 这样，您的程序将从内存中删除，并将从您的文件重新加载。

可以与整个Stata社区分享您的新命令也很简单。查看常见问题解答[如何与Stata用户共享新命令？](https://www.stata.com/support/faqs/resources/sharing-a-command/)

**典型流程归纳**

这是一个典型的自动化过程：

   1.编写特定问题的解决方案。
​      a.发现自己一遍又一遍地复制那些代码。
​      b. 问自己从一个问题到另一个问题的变化。

   2.编写一个do文件，将那些变化的内容作为参数。
​      a. 提炼。
​      b. 测试
​      C.重复2a和2b，直到满意

   3可以把你的do文件变成一个ado文件。

   4.可以与同事分享您的ado文件。

   5.可以与整个Stata社区分享您的ado文件。

恭喜，您现在可以自动执行Stata中的常见任务。无论你是否愿意，你都在成为一名程序员。

如果你对我们迄今为止所做的事情感到满意，这将是退出阅读的好时机。

**附录：一个补充**

您可能不希望自动将**N**附加到原始变量名称的末尾以指定标准化变量。也许你想使用不同的字母，或者可能是一组字符，比如**_norm**。或者您可能更喜欢后缀的前缀。甚至也许你想要两个。

我们可以实现这一点。



**normalize.ado**（b）



```
program normalize
    version 15.1

    syntax varlist [if] [in] [ , prefix(name) suffix(name) ]

    foreach var in `varlist' {
        summarize `var' `if' `in'
        generate `prefix'`var'`suffix' = (`var' - r(mean)) / r(sd)   `if' `in'
    }
end
```

从版本（a）到版本（b），我们所做的只是把

```
syntax varlist [if] [in]
```

改为

```
syntax varlist [if] [in] [, prefix(name) suffix(name)]
```

并改变

```
generate `var'N = ...
```

至

```
generate `prefix'`var'`suffix' = ...
```

让我们先来理解**syntax**行的变化

方括号表示所含部分并非强制选项;

但是，如果他们决定输入内容，他们必须先输入逗号**，** 。然后，他们可以键入 **前缀（ pstuff **）或 **后缀（**sstuff**）** ，或两者同时。如果他们键入**前缀（***pstuff* **）**，那么局部暂元**前缀**将包含他们在括号中*输入的内容*。局部暂元**后缀**将包含用户在**后缀**选项的括号中键入的内容。

我们编写**语法**命令时必须小心。因为我们写了**（name）**而不是**（string）**，所以用户不能在括号中键入任何内容。无论他们输入什么，都必须是合法的Stata变量名称。我们打算使用键入的作为变量名称的前缀或后缀，以便该字符串本身不包含任何在变量名称中非法的内容。

现在，一下代码什么意思呢？

```
generate `prefix'`var'`suffix' = ...
```



暂元**'prefix'**和**'suffix'**将扩展为用户在**prefix**和**suffix**选项中输入的任何内容。我们的新变量名将带有用户输入入的前缀和后缀。

使用我们的新ado文件，我们现在可以输入类似的内容

```
. normalize x1 x2 x3 x4 , prefix(norm_of_)
. normalize x* , prefix(norm_of_)
```

第一行创建四个新变量：**norm_of_x1**，**norm_of_x2**，**norm_of_x3**，**norm_of_x4**。我不喜欢这些名字，但它们含义清晰。除非你在考虑矩阵。有时，这些称为标准化变量，因此您可能更喜欢**前缀（std_）**。

在第二行中，**x \***匹配以**x**开头的所有变量。它们中的每一个都将被标准化，并使用指定的前缀**norm_of_**创建一个新变量。

你可能已经注意到了一个潜伏的bug。如果用户既不键入 **前缀（）** 也不键入 **后缀（）** 选项，那么 **`prefix'**和**`suffix'** 都将为空。我们的 **generate** 命令将尝试创建一个与原始变量同名的变量。那......是语法错误。

避免该错误的一种方法是默认为我们用**“N”** 后缀新变量的原始行为。我们通过在**语法**行下面添加以下三行来实现这一点，

```
if "`prefix'`suffix'" == "" {
    local suffix "N"
}
```

他们是说，如果前缀和后缀(**`prefix’`suffix’**) 都为空，则将**“N”**分配给后缀。

另一个不错的改进是为我们的新变量添加标签。这是一种可能性：

```
label variable `prefix'`var'`suffix' "`var' normalized"
```

我们将在**generate**命令之后添加。在**for**循环中。

这就是程序变长的方式。您可以改进它们，并添加功能。坚持这一点，你很快就会编写一些复杂代码来难倒你的同事。

>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com


---
![欢迎加入Stata连享会(公众号: StataChina)](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")









