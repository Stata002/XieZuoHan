从0开始stata之数据标签与排序
---
> 作者：谢作翰 |  连玉君 | ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> **编者按：** 从本期开始，Stata 连享会将推出「Stata小白系列」推文，介绍数据导入、命令语法等 Stata 入门知识，以帮助各位尽快掌握 Stata 的基本操作。


---
  **目录**
一  数据调入

0 概念区分
1.  调入本地标准文件
  - 调入全部数据
  - 调入部分变量
  - 调入部分样本
  - 调入具有某些特征的样本
 2.  调入网络文件
  - 调用 stata 自带数据库——` sysuse `命令
  - 调入stata 手册在线数据 —— ` webuse set `命令 
  -  Wooldridge， Greene 等经典教科书范例数据获取——` bcuse `命令
  -  调用联邦储备经济数据库 (FRED)——` freduse `&` import fred `

 3.  大杀器` copy `命令——从下载地址直接储存文件或复制网页文本信息
 4.  调入ASCII数据 
   - 读取ASCII数据 ——  `insheet`命令
   - 读取无固定格式命令 —— `infile`命令
   - 读取固定格式数据—— ` infix`
5. 调入excel数据——` import excel ` &` xls2dta `

二  数据标签与排序
1. 数据标签
 -  为数据库添加标签
  - 为变量添加标签
  - 为数值添加标签
2. 数据排序—— `sort`& `gsort`

三  数据拆分与合并 

- 数据拆分

  1. 数据横向拆分—— `keep`& `drop`
  2.  数据纵向拆分—— `keep`& `drop`
  3.  一步到位保存数据子集—— `savesome`
- 数据合并
  1. 数据的横向合并—— `merge`
  2. 数据的纵向合并—— `append`
  3. 对多个 `csv`文件纵向合并—— `csvconvert`

四  长宽数据转换—— `reshape`命令
 - 宽数据转为长数据
- 长数据转为宽数据
---
数据文件：链接: https://pan.baidu.com/s/1qXRh9EG  密码: 5ltw

---
##  数据标签
为了让用户更加清楚地了解数据的来源、变量的含义、观测值的解释等相关内容， `Stata`可以为数据、变量、观测值添加标签，其实标签就是对相关数据的解释。最常用的标签方法有三类：一是为数据库添加标签，二是为变量添加标签，三是为观测值添加标签

1. **为数据库添加标签**

*命令*： `label data ["label"]`

在这个命令语句中， `label data`是为数据库添加标签的命令语句， ` ["label"]`代表所要添加的标签的内容

2. **为变量添加标签** 
 
*命令*： `label variable varname ["label"]`
 `label variable`是为变量添加标签的命令语句， `varname`代表所要添加标签的变量名称， `["label"]`代表所要添加的标签的内容。

 
3. **为数值添加标签**

对于数值型的分类变量，单纯通过数值很难判断其含义，例如用 0 和 1 区分性别，但是很难区分 0 是代表男性还是女性，这时如果给数值添加标签，就方便用户理解了。

为数值添加标签是通过两步来完成的，**第一步是定义数值标签**:

*命令为*：
```stata
 label define lblname  "label" [ "label" ...] [, add modify nofix]
 ```

*说明*： `label define`是定义数值标签的命令语句 `lblname`代表所要定义的数值标签的名称， `#`代表所要定义的数值， `"label"`代表所要添加的标签的内容。需要用户注意的是后方 `options`的内容，其中 `add`的作用是添加标签内容， `modify`的作用是对已存在的标签内容做修改， `nofix`的作用是要求` Stata `不为标签的内容而改变原变量的存储容量
   第二步是**将所定义的数据标签与相关变量结合**，
*基本命令为*：

` label values varname [lblname] [, nofix] `

在这个命令语句中， `label values`是将定义的数据标签与相关变量结合的语句， `varname`代表将要添加标签的变量名称， `[lblname]`代表刚刚定义的数据标签名称。

  通过下面的例子——给 `usaauto.dta`数据库添加三类标签，为大家介绍标签的使用方法， `usaauto`数据文件如表所示
![usaauto](http://upload-images.jianshu.io/upload_images/1844375-bbf8f7146f332c38.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 
导入数据：
```stata
 use "D:\360Downloads\PanDownload\操作数据\教程数据\chap02\usaauto.dta", clear
```

①为整个数据库添加标签“1978年美国汽车产业的横截面数据”

其命令如下：

`label data “1978年美国汽车产业的横截面数据”`

这个命令语句中 `label data`表示为数据库添加标签，`“1978年美国汽车产业的横截面数据”`指明了标签的内容。
 ![数据库标签](http://upload-images.jianshu.io/upload_images/1844375-30096efe4cd3ba2a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

②为变量 `make`和 `mpg`添加标签“品牌”和“每加仑油行使里程数”，

其命令如下：
```
label variable make “品牌”
label variable mpg “每加仑油行使里程数”
```
 `label variable`表示为变量添加标签， `make` “品牌”表示要添加标签的变量和标签的内容。

 ![变量标签](http://upload-images.jianshu.io/upload_images/1844375-676a188fc107257f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

③  为数值添加标签

变量 `foreign`为分类变量，**0** 代表国产,**1** 代表进口，为了便于观察，为**0** 和**1** 添加标签，

具体命令为：
  ```stata
label define foreignlabel 0 “Domestic” 1 “Foreign”
label values foreign foreignlabel
```


 `label define`表示定义标签的内容， `foreignlabel`表示标签的名称, `0  “Domestic” 1 “Foreign”`表示定义的规则，数字**0**的标签是**国产车**，数字**1**的标签是**进口车**。
![设置前](http://upload-images.jianshu.io/upload_images/1844375-41c8a3e2906169cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![设置后](http://upload-images.jianshu.io/upload_images/1844375-7eeb8e24ff90d70f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

 `label values`表示将定义的标签与变量结合的命令，所要结合的变量为 `foreign`，结合的标签为 `foreignlabel`。标签添加完成以后，可以通过 `label dir`命令，查看已经建立标签的相关内容。

## 数据排序

为了处理数据的方便，有时需要对数据进行排序处理，排序的命令有主要有两个，一个是 `sort`命令，一个是 `gsort`命令

 `sort`命令的基本语句是：

 `sort varlist [in] [, stable]`

在这个命令语句中， `sort`是基本命令， `varlist`代表将要进行排序的变量名称， `[in]`代表排序的范围， `[, stable]`的含义是如果两个观测值相同，其顺序保持与原数据相同。
 `gsort` 命令的基本语句是：

```stata
 gsort [+|-] varname [[+|-] varname ...] [, generate(newvar) mfirst]
```

其中需要说明的内容有两点：
*  `[+]`表示按升序排列，是 `Stata`默认的排列方式，` [-]`表示按降序排列
* `generate(newvar)`表示排序之后生成新的变量， `mfirst`表示将缺失值排在最前面。

将 `usaauto`数据文件中的观测值按变量 `price`由小到大排列，这个操作可以用 `sort`命令完成，具体操作如下：

 `sort price`

当然也可以用gsort命令完成，具体操作如下：

`gsort + price`
---
> 本文中所用数据文件下载地址：

数据文件：链接: [https://pan.baidu.com/s/1qXRh9EG](https://pan.baidu.com/s/1qXRh9EG)  密码: 5ltw


>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 特别说明
文中包含的链接在微信中无法生效。请点击本文底部左下角的`【阅读原文】`，转入本文`【简书版】`。


---
![Stata连享会二维码](http://upload-images.jianshu.io/upload_images/7692714-8a75eac8a9b0a525.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")