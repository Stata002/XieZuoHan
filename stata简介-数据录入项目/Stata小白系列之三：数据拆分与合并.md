
> 作者：谢作翰 |  连玉君 | ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> **编者按：** 从本期开始，Stata 连享会将推出「Stata小白系列」推文，介绍数据导入、命令语法等 Stata 入门知识，以帮助各位尽快掌握 Stata 的基本操作。


---  
  目录

一  数据调入

0 概念区分
1.  调入本地标准文件
  - 调入全部数据
  - 调入部分变量
  - 调入部分样本
  - 调入具有某些特征的样本
 2.  调入网络文件
  - 调用 stata 自带数据库——` sysuse `命令
  - 调入stata 手册在线数据 —— ` webuse set `命令 
  -  Wooldridge， Greene 等经典教科书范例数据获取——` bcuse `命令
  -  调用联邦储备经济数据库 (FRED)——` freduse `&` import fred `

 3.  大杀器` copy `命令——从下载地址直接储存文件或复制网页文本信息
 4.  调入ASCII数据 
   - 读取ASCII数据 ——  `insheet`命令
   - 读取无固定格式命令 —— `infile`命令
   - 读取固定格式数据—— ` infix`
5. 调入excel数据——` import excel ` &` xls2dta `

二  数据标签与排序
1. 数据标签
 -  为数据库添加标签
  - 为变量添加标签
  - 为数值添加标签
2. 数据排序—— `sort`& `gsort`

三  数据拆分与合并 

- 数据拆分

  1. 数据横向拆分—— `keep`& `drop`
  2.  数据纵向拆分—— `keep`& `drop`
  3.  一步到位保存数据子集—— `savesome`
- 数据合并
  1. 数据的横向合并—— `merge`
  2. 数据的纵向合并—— `append`
  3. 对多个 `csv`文件纵向合并—— `csvconvert`

四  长宽数据转换—— `reshape`命令
 - 宽数据转为长数据
- 长数据转为宽数据
---
数据文件：链接: https://pan.baidu.com/s/1qXRh9EG  密码: 5ltw

---
##  数据拆分
1. **数据的横向拆分**
原始数据有时包含过多的变量，但在实际应用中可能根据需要将原始数据拆分为不同的数据表，这时就要实现数据的横向拆分。数据的横向拆分用到的两个命令为 `drop`和 `keep`, `drop`命令是用来**删除某些变量和观测值**的，基本命令如下：

```stata
drop varlist [if] [in]
```

 `keep`命令是用来**保留某些变量和观测值**的，基本命令如下：

```stata
 keep varlist [if] [in]
```
 *举例说明* ： `water`数据文件包含四个变量，分别是 `year`, `capital`、 `production`、 `labor`，将 `water`数据文件拆分为两个数据文件，一个数据文件包含 `year`和 `production`两个变量，命名 `wateroutput`，一个数据文件包含 `year`、 `capital`和 `labor`三个变量，命名为 `waterinput`。这个操作的具体命令如下：
```stata
use c:\data\water,clear
drop capital labor
save c:\data\wateroutput, replace
```
以上命令，第一个命令实现了原数据文件的打开，第二个命令删除了变量
 `capital`和 `labor`，第三个命令实现了存储，新文件的名字为
 `wateroutput`，并用 `replace`命令替换了原有数据。
```stata
	use c:\data\water,clear
	keep year capital labor
	save c:\data\waterinput, replace
```
以上命令，第一个命令实现了原数据文件的打开，第二个命令保留了变量
 `capital`和 `labor`，第三个命令实现了存储，新文件的名字为 `waterinput`，并用 `replace`命令替换了原有数据
 
2. **数据的纵向拆分**

原始数据有时包含过多的样本观测值，但在实际应用中可能根据需要将其按某种特征拆分为不同的数据表，这是就要实现数据的纵向拆分。数据的纵向拆分用到的主要命令还是 `drop`和 `keep`。

例如将 `usaauto`数据文件拆分为两个数据文件，一个数据文件叫
 `domesticauto`，只包含国产车的相关内容，一个数据文件叫 `foreignauto`，只包含进口车的相关内容，具体操作如下：
```stata
use c:\data\usaauto,clear
drop if foreign==1
save c:\data\domesticauto, replace 
```
以上命令完成了第一个数据文件的建立，第一个命令完成了原始数据文件的打开，第二个命令删除 `foreign`变量为 1 的数据，第三个命令存储新的数据文件，名称为 `domesticauto`，并用 `replace`命令替换了原有数据

第二个数据文件建立的命令如下所示：
```
	use c:\data\usaauto,clear
	keep if foreign==1
	save c:\data\foreignauto, replace
```
以上命令完成了第二个数据文件的建立，第一个命令完成了原始数据文件的打开，第二个命令保留了 `foreign`变量为 1 的数据，第三个命令存储新的数据文件，名称为 `foreignauto`，并用 `replace`命令替换了原有数据

 3. **一步到位保存数据子集—— `savesome`**
在一个数据集中使用 `stata`自带的 `keep`和 `drop`命令构建所需要的子集时筛选和保存需要用到两个不同的命令。接下来介绍的外部命令
 `savesome`可以一步到位保存数据子集。
命令安装：
```stata
 help savesome 
```
从 `stata`帮助界面中找到下载链接
命令语句：
```
    savesome [varlist] [if exp] [in range] using filename [, old save_options]
```
命令解释：
 `savesome`命令分为四个部分： 第一个部分是 `if `语句，用来筛选符合条件的观测；第二部分是变量名称，如果要选取数据集中特定变量作为子集，直接将变量名称写上；第三部分 `using filename` 表示要保存的子集文件名及文件路径；第四部分 `old` 为可选参数，添加参数后数据子集可支持较早版本的 `stata`

举例说明：
 我们使用` stata `自带的` auto `数据集：
1. 选择数据集中进口车
```
savesome if foreign=1 using foreignauto.dta
```
2. 从数据集中选择 `make price foregn`构建子集，保存为 `smallauto`
``` 
savesome make price foregn using smallauto.dta 
```


##  数据合并
1. **数据的横向合并**

数据的横向合并是横向拆分的逆操作，但是其要比拆分复杂，因为合并时要实现同一个体数据的对接，而不能出现对接错误的情况。所以在横向合并之前最好先对数据进行排序处理，然后实现合并。合并所使用的命令语句为 `merge `，具体语句如下所示：

```
merge [varlist] using filename [filename ...] [, options]
``` 

 `merge `为合并的命令语句， `[varlist] `代表合并进去的新变量， `using filename`指的是所要与原文件合并的文件路径， `options`包含较多的功能，表 2.11 显示了其具体内容。
 ![](http://upload-images.jianshu.io/upload_images/1844375-a34f88966686e4a8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

*举例*：利用横向拆分实验中生成的数据文件 `waterinput` 和 `wateroutput`实现数据的横向合并，匹配变量为 `year`，生成新的数据文件命名为 `waternew`。这个操作的命令为：
```
use c:\data\wateroutput, clear
sort year
save c:\data\wateroutput, replace
use c:\data\waterinput, clear
sort year
merge year using c:\data\wateroutput
save c:\data\waternew, replace
```
在以上命令语句中，第一个命令语句实现了 `wateroutput`数据文件的打开，第二个命令语句将文件按年份变量进行排序，第三个命令语句保存了排序之后的数据文件，第四个命令语句实现了 `waterinput`数据文件的打开，第五个命令语句将此数据按年份变量进行排序，第六个命令语句按年份变量将 `wateroutput`文件合并到 `waterinput`文件中，第七个命令语句保存合并之后的数据文件。
2. **数据的纵向合并**

数据的纵向合并为数据纵向拆分的逆操作，使用的主要命令为 `append`命令，具体语句如下：

```
 append using filename [, options] 
```

在这个命令语句中， `append`是进行纵向合并的命令语句， `using filename`是进行纵向合并的文件路径， `[, options]`的内容与 `merge`相似，但更为简化。

例如，利用纵向拆分实验中生成的数据文 `domesticauto`和 `foreignauto`实现数据的纵向合并，生成的数据文件命名为 `usaautonew`。这个操作的命令为：
```
use c:\data\domesticauto, clear
append using c:\data\foreignauto
save c:\data\usaautonew, replace
```
在以上命令中，第一个命令语句打开了原始数据文件，第二个命令将 `foreignauto`文件合并到 `domesticauto`文件中，第三个命令语句存储了合并后的数据文件。
3. **一次合并多个csv文件—— `csvconvert`**
 `csvconvert` 命令用于将多个 `csv`格式文件合并为一个 `.dta`格式文件，比较适合处理具有时间周期性特点的变量。
命令：
``` 
 csvconvert input_directory , replace [options] 
```
该命令有三个参数：

| 参数名        | 功能         | 
| :------------- |:-------------:| 
| output_file(file_name)   |设置输出文件名 | 
|  output_dir(output_directory)  |设置输出路径   | 
| input_file(.csv file list) | 选择要合并的文件   |  

命令下载：
```
help csvconvert
``` 
以命令配套的四个世界银行文件为例，首先看文件结构 
```
dir C:\Uers\Administrator\Desktop\stata学习文件\worldbank\*.csv
```
![](http://upload-images.jianshu.io/upload_images/1844375-e5cf9482ce01b6b2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![wb2010.csv](http://upload-images.jianshu.io/upload_images/1844375-f7bbe40ed5d92e78.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

使用 `csvconvert`命令将四个年度数据纵向合并
```  
csvconvert  C:\Users\Administrator\Desktop\stata学习文件\worldbank, replace
```

![默认情况下合并后的文件名为output.dta ](http://upload-images.jianshu.io/upload_images/1844375-af672ac5039dd20f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


要合并的四个子文件必须在同一个文件夹中，默认情况下， `csvconvert`命令会合并文件夹中所有文件 ，可以用 `note` 命令核对所合并的文件情况 .用 `input_file`参数选择所有合并的文件名，在此，我们只选择2008年度和2009年度的数据
```
csvconvert C:\Users\Administrator\Desktop\stata学习文件\worldbank
, replace input_file(wb2008.csv wb2009.csv)
```

> 本文中所用数据文件下载地址：

数据文件：链接: [https://pan.baidu.com/s/1qXRh9EG](https://pan.baidu.com/s/1qXRh9EG)  密码: 5ltw


>#### 关于我们
- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [【简书-Stata连享会】](http://www.jianshu.com/u/69a30474ef33) 和 [【知乎-连玉君Stata专栏】](https://www.zhihu.com/people/arlionn)。可以在**简书**和**知乎**中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 推文中的相关数据和程序，以及 [Markdown 格式原文](https://gitee.com/arlionn/jianshu) 可以在 [【Stata连享会-码云】](https://gitee.com/arlionn) 中获取。[【Stata连享会-码云】](https://gitee.com/arlionn) 中还放置了诸多 Stata 资源和程序。如 [Stata命令导航](https://gitee.com/arlionn/stata/wikis/Home) ||  [stata-fundamentals](https://gitee.com/arlionn/stata-fundamentals) ||  [Propensity-score-matching-in-stata](https://gitee.com/arlionn/propensity-score-matching-in-stata) || [Stata-Training](https://gitee.com/arlionn/StataTraining) 等。


>#### 联系我们
- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 特别说明
文中包含的链接在微信中无法生效。请点击本文底部左下角的`【阅读原文】`，转入本文`【简书版】`。


---
![Stata连享会二维码](http://upload-images.jianshu.io/upload_images/7692714-8a75eac8a9b0a525.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")


