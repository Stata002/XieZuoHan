Stata小白系列之四：长宽数据转换
---
> 作者：谢作翰 |  连玉君 | ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)) 

> **编者按：** 从本期开始，Stata 连享会将推出「Stata小白系列」推文，介绍数据导入、命令语法等 Stata 入门知识，以帮助各位尽快掌握 Stata 的基本操作。


---
**目录**

0 概念区分
1.  调入本地标准文件
  - 调入全部数据
  - 调入部分变量
  - 调入部分样本
  - 调入具有某些特征的样本
 2.  调入网络文件
  - 调用 stata 自带数据库——` sysuse `命令
  - 调入stata 手册在线数据 —— ` webuse set `命令 
  -  Wooldridge， Greene 等经典教科书范例数据获取——` bcuse `命令
  -  调用联邦储备经济数据库 (FRED)——` freduse `&` import fred `

 3.  大杀器` copy `命令——从下载地址直接储存文件或复制网页文本信息
 4.  调入ASCII数据 
   - 读取ASCII数据 ——  `insheet`命令
   - 读取无固定格式命令 —— `infile`命令
   - 读取固定格式数据—— ` infix`
5. 调入excel数据——` import excel ` &` xls2dta `

二  数据标签与排序
1. 数据标签
 -  为数据库添加标签
  - 为变量添加标签
  - 为数值添加标签
2. 数据排序—— `sort`& `gsort`

三  数据拆分与合并 

- 数据拆分

  1. 数据横向拆分—— `keep`& `drop`
  2.  数据纵向拆分—— `keep`& `drop`
  3.  一步到位保存数据子集—— `savesome`
- 数据合并
  1. 数据的横向合并—— `merge`
  2. 数据的纵向合并—— `append`
  3. 对多个 `csv`文件纵向合并—— `csvconvert`

四  长宽数据转换—— reshape 命令
 - 宽数据转为长数据
- 长数据转为宽数据
---
数据文件：链接: https://pan.baidu.com/s/1qXRh9EG  密码: 5ltw

---
##  长宽数据转换—— `reshape`命令

在**面板数据**中，如果包含两个**标识变量**，则数据有两种表现形式，一种是长数据，一种是宽数据。

在长宽数据的转换中，所使用到的命令为 
 `reshape`，具体命令语句为：
```
reshape long stubnames, i(varlist) [options]
reshape wide stubnames, i(varlist) [options]
```
其中，
- `long `表示将宽数据转化为长数据
- `wide`表示将长数据转化成宽数据 
- `stubnames`表示需要转化的变量名称前缀
- `i(varlist)`表示识别变量
- `options` 最常用的为 `j(varname [values])`，它表示用来进行长宽变换的变量名称，通常为时间变量。

**宽数据** &rarr; **长数据** 的命令为：
```
use c:\data\widedata, clear
 
reshape long english science, i(number name) j(year)
 ```
 ![宽数据](http://upload-images.jianshu.io/upload_images/1844375-8866dff041f3f560.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![转换过程](http://upload-images.jianshu.io/upload_images/1844375-50dcd29621ed7a0c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![长数据](http://upload-images.jianshu.io/upload_images/1844375-ff32fcb94c968c4d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


 `reshape long` 表示将宽数据转化成长数据的命令语句， `english`和 `science`是要转化变量名称的前缀，也是即将生成变量的名称， `i(number name)`表示识别变量，即按学号`number`和名称 `name`区分所有观测值， `j(year)`表示将要长宽转换的变量，一般是按年份进行转化。

**长数据** &rarr; **宽数据** 的命令为：
```
use c:\data\longdata, clear
 
reshape wide english science, i(number name) j(year)
 ```
 ![长数据](http://upload-images.jianshu.io/upload_images/1844375-840d40971cffe15d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![转换过程](http://upload-images.jianshu.io/upload_images/1844375-227f3e4f2b906883.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![宽数据](http://upload-images.jianshu.io/upload_images/1844375-97238d83512f9623.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

具体说明如下：
- `reshape wide` 表示长数据转成宽数据命令语句
- `english` 和 `science` 表示将要转化的变量名称的前缀，也即将要生成的变量的名称
- `i(number name)` 表示识别变量，即按学号 **number** 和名称 **name** 区分所有观测值
-  `j(year)` 表示将要长宽转换的变量，一般是按年份进行转化

---
> 本文中所用数据文件下载地址：

数据文件：链接: [https://pan.baidu.com/s/1qXRh9EG](https://pan.baidu.com/s/1qXRh9EG)  密码: 5ltw



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)
![](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)





>#### 特别说明
文中包含的链接在微信中无法生效。请点击本文底部左下角的`【阅读原文】`，转入本文`【简书版】`。


---
![Stata连享会二维码](http://upload-images.jianshu.io/upload_images/7692714-8a75eac8a9b0a525.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")